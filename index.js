let http = require('http');

//Mock Database
let mockDatabase = [
	{
	"firstName": "Mary Jane",
	"lastName": "Dela Cruz",
	"mobileNo": "09123456789",
	"email": "mjdelacruz@mail.com",
	"password": "123"
	},
	{
	"firstName": "John",
	"lastName": "Doe",
	"mobileNo": "09123456789",
	"email": "jdoe@mail.com",
	"password": "123"
	}
]

http.createServer(function(req,res){

	if(req.url == "/register" && req.method == "POST"){
		let requestBody = '';

		req.on('data',function(data){
			requestBody += data;
		})

		req.on('end',function(){
			console.log(typeof requestBody)

			requestBody = JSON.parse(requestBody);

			let newUser = {
				"firtName": requestBody.firstName,
				"lastName": requestBody.lastName,
				"mobileNo": requestBody.mobileNo,
				"email": requestBody.email,
				"password": requestBody.password
			}

			mockDatabase.push(newUser)
			console.log(mockDatabase)

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}


}).listen(4000);

console.log('s27 activity server running at localhost:4000');